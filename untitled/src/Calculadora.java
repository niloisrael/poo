public class Calculadora {
    private Double primero;
    private Double segundo;
    private Double resultado;
    public double sumar(){
        return primero+segundo;
    }
    public double multiplicar(){
        return primero*segundo;
    }
    public double dividir(){
        return primero/segundo;
    }
    public static void main(String[] args){
        Calculadora a = new Calculadora();
        a.primero=5.0;
        a.segundo=5.8;
        a.resultado=a.sumar();
        System.out.println(a.resultado);
        Calculadora b = new Calculadora();
        b.primero=5.0;
        b.segundo=5.8;
        b.resultado=b.multiplicar();
        System.out.println(b.resultado);
        Calculadora c = new Calculadora();
        c.primero=5.0;
        c.segundo=5.8;
        c.resultado=c.dividir();
        System.out.println(c.resultado);
    }
}

